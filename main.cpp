#include <deque>
#include <map>
#include <windows.h>

typedef int( __stdcall *CLEO_RegisterOpcode_t )( WORD, int( __stdcall * )( void * ) );
typedef int( __stdcall *CLEO_GetIntOpcodeParam_t )( void * );
typedef void( __stdcall *CLEO_SetIntOpcodeParam_t )( void *, int );
typedef void( __stdcall *CLEO_SkipOpcodeParams_t )( void *, int );
typedef int( __stdcall *CLEO_GetOperandType_t )( void * );
typedef void( __stdcall *CLEO_SetThreadCondResult_t )( void *, int );

namespace {
	CLEO_RegisterOpcode_t	   CLEO_RegisterOpcode		= nullptr;
	CLEO_GetIntOpcodeParam_t   CLEO_GetIntOpcodeParam	= nullptr;
	CLEO_SetIntOpcodeParam_t   CLEO_SetIntOpcodeParam	= nullptr;
	CLEO_SkipOpcodeParams_t	   CLEO_SkipOpcodeParams	= nullptr;
	CLEO_GetOperandType_t	   CLEO_GetOperandType		= nullptr;
	CLEO_SetThreadCondResult_t CLEO_SetThreadCondResult = nullptr;

	std::map<void *, std::deque<int>> stacks;

	int __stdcall op_push( void *thread ) {
		auto typeId = CLEO_GetOperandType( thread );
		if ( typeId >= 9 ) {
			CLEO_SkipOpcodeParams( thread, 1 );
			CLEO_SetThreadCondResult( thread, 0 );
		} else {
			stacks[thread].push_back( CLEO_GetIntOpcodeParam( thread ) );
			CLEO_SetThreadCondResult( thread, 1 );
		}
		return 0;
	}

	int __stdcall op_pop( void *thread ) {
		auto typeId = CLEO_GetOperandType( thread );
		if ( ( typeId != 2 && typeId != 3 && typeId != 7 && typeId != 8 ) || stacks[thread].empty() ) {
			CLEO_SkipOpcodeParams( thread, 1 );
			CLEO_SetThreadCondResult( thread, 0 );
		} else {
			CLEO_SetIntOpcodeParam( thread, stacks[thread].back() );
			stacks[thread].pop_back();
			CLEO_SetThreadCondResult( thread, 1 );
		}
		return 0;
	}
} // namespace

BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {
		auto cleo				 = GetModuleHandleA( "CLEO.asi" );
		CLEO_RegisterOpcode		 = (CLEO_RegisterOpcode_t)GetProcAddress( cleo, "_CLEO_RegisterOpcode@8" );
		CLEO_GetIntOpcodeParam	 = (CLEO_GetIntOpcodeParam_t)GetProcAddress( cleo, "_CLEO_GetIntOpcodeParam@4" );
		CLEO_SetIntOpcodeParam	 = (CLEO_SetIntOpcodeParam_t)GetProcAddress( cleo, "_CLEO_SetIntOpcodeParam@8" );
		CLEO_SkipOpcodeParams	 = (CLEO_SkipOpcodeParams_t)GetProcAddress( cleo, "_CLEO_SkipOpcodeParams@8" );
		CLEO_GetOperandType		 = (CLEO_GetOperandType_t)GetProcAddress( cleo, "_CLEO_GetOperandType@4" );
		CLEO_SetThreadCondResult = (CLEO_SetThreadCondResult_t)GetProcAddress( cleo, "_CLEO_SetThreadCondResult@8" );

		if ( !CLEO_RegisterOpcode ) return FALSE;
		if ( !CLEO_GetIntOpcodeParam ) return FALSE;
		if ( !CLEO_SetIntOpcodeParam ) return FALSE;
		if ( !CLEO_SkipOpcodeParams ) return FALSE;
		if ( !CLEO_GetOperandType ) return FALSE;
		if ( !CLEO_SetThreadCondResult ) return FALSE;

		CLEO_RegisterOpcode( 0x0C9A, &op_push );
		CLEO_RegisterOpcode( 0x0C9B, &op_pop );
	}

	return TRUE;
}
